import React from 'react'
import './SearchStyles.css'

import Gold from '../../assets/gold.png'

function Search() {
    return (
        <div name='book' className='search'>
            <div className="container">
                <div className="left">
                    <h2>PROFITEZ DE NOTRE TARIF PROMOTIONEL A PARTIR DE 100 000MGA</h2>
                    <p>Venez découvrir des vacances innoubliable tout compris et luxieux à Nosy-Be.
Nos balnéaires de luxe, situés le long des plus beaux décors tropicaux et des plages exquises du canal de Mozambique,Nosy Iranja, Nosy Tany kely, les orgues baltiques, l'Ile Mitsio et d'autres richesses sous marines et terrestre, proposent des repas gastronomiques illimités, des bars uniques servant des liqueurs et des vins de qualité supérieure, et chaque terre et eau sport, y compris les seance de pongeons et pêches au larges. Si vous planifiez votre lune de miel, nous avons un forfait spécialement pour vous.</p>
                    <div className="search-col-2">
                        <div className="box">
                            <div>
                                <img src={Gold} alt="/" style={{ marginRight: '1rem' }} />
                            </div>
                            <div>
                                <h3>WORLD'S LEADING</h3>
                                <p>ALL-INCLUSIVE COMPANY FOR 20 YEARS IN-A-ROW</p>
                            </div>
                        </div>
                        <div className="box">
                            <div>
                                <h3>NO ONE INCLUDES MORE</h3>
                                <p>ALL-INCLUSIVE COMPANY FOR 20 YEARS IN-A-ROW</p>
                                <button>View Packages</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="right">
                    <div className="promo">
                        <h4 className="save">BENEFICIEZ UNE REDUCTION DE 10%</h4>
                        <p className="timer">12 HOURS LEFT!</p>
                        <p className="offers">ALL OFFRES</p>
                    </div>
                    <form>
                        <div className="input-wrap">
                            <label>Destinations</label>
                            <select>
                                <option value="1">Nosy Iranja</option>
                                <option value="1">Nosy TaniHely</option>
                                <option value="1">Iles aux tortues</option>
                                <option value="1">Tsarabanjina</option>
                                <option value="1">Orgues Basaltiques</option>
                                <option value="1">Mon Passeau</option>
                                <option value="1">Arbre Sacrée</option>
                                <option value="1">Nosy Mitsio</option>
                            </select>
                        </div>
                        <div className="date">
                            <div className="input-wrap">
                                <label>Check-In</label>
                                <input type="date" />
                            </div>
                            <div className="input-wrap">
                                <label>Check-Out</label>
                                <input type="date" />
                            </div>
                        </div>
                        <button>Rates & Availabilities </button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default Search
