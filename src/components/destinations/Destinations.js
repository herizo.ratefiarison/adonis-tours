import React from 'react'
import './DestinationsStyles.css'

// import BoraBora from '../../assets/borabora.jpg'
// import BoraBora2 from '../../assets/borabora2.jpg'
// import Maldives from '../../assets/maldives.jpg'
// import Maldives2 from '../../assets/maldives2.jpg'
// import KeyWest from '../../assets/keywest.jpg'
import Monpasseau from '../../assets/mon-passeau.jpg'
import Iranjakely from '../../assets/Iranja-kely.jpg'
import Mitsio from '../../assets/archipelle-mitsio.jpg'
import Tsarabanjina from '../../assets/paysage-tsarabanjina.jpg'
import Basaltes from '../../assets/orgues-basaltiques.jpg'

function Destinations() {
    return (
        <div name='destinations' className='destinations'>
            <div className="container">
                <h1>All-Inclusive Services</h1>
                <p>Nos Sites Touristiques les plus marquant</p>
                <div className="img-container">
                    <img className='span-3 image-grid-row-2' src={Iranjakely} alt="/"/>
                    <img src={Monpasseau} alt="/"/>
                    <img src={Mitsio} alt="/"/>
                    <img src={Tsarabanjina} alt="/"/>
                    <img src={Basaltes} alt="/"/>
                </div>
            </div>
        </div>
    )
}

export default Destinations
